#!/bin/bash

export PWD=`pwd`

# Gracefully stop existing container (if any)
if docker ps | grep -q 'vcg-api-server'; then
    docker exec -i vcg-api-server service supervisor stop && \
    docker exec -i vcg-api-server redis-cli shutdown save && \
    docker-compose stop
fi

if docker volume inspect vcg_server_database 2> /dev/null | grep -q 'vcg_server_database'; then
    export DATABASE_INITIALIZED=1
else
    export DATABASE_INITIALIZED=0
fi
