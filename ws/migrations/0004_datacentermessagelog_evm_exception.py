# Generated by Django 2.0.5 on 2018-05-10 10:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ws', '0003_auto_20180510_0918'),
    ]

    operations = [
        migrations.AddField(
            model_name='datacentermessagelog',
            name='evm_exception',
            field=models.BooleanField(default=False, help_text='If the ethereum client threw an exception'),
        ),
    ]
