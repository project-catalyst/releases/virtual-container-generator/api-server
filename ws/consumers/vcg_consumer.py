import logging
import uuid

from asgiref.sync import async_to_sync
from channels.generic.websocket import JsonWebsocketConsumer
from django.utils import timezone

from ws.models import DatacenterClient, DatacenterMessageLog

log = logging.getLogger(__name__)


class VCGConsumer(JsonWebsocketConsumer):
    """Channels consumer that handles websocket connection coming from DCs.
    """
    def connect(self):
        """Fired when the vcg_api client websocket connects.
        """
        headers = dict(self.scope['headers'])
        wallet_address = headers[b'wallet'].decode('utf-8')
        self.dc_name = self.scope['url_route']['kwargs']['dc_name']
        self.dc_client = DatacenterClient.objects.filter(address=wallet_address, name=self.dc_name)

        # Reject unknown clients
        if not self.dc_client.exists():
            log.warning('Rejecting client with name={} and address={}'.format(self.dc_name, wallet_address))
            self.close()
            self.dc_client = None
            return

        # Update the client details
        self.dc_client.update(
            channel_name=self.channel_name, last_connected=timezone.now(), status='connected'
        )
        self.dc_client = self.dc_client.first()

        async_to_sync(self.channel_layer.group_add)(
            self.dc_client.group_name,
            self.channel_name
        )
        self.accept()
        log.info("Datacenter `{}` connected.".format(self.dc_client.name))

    def disconnect(self, close_code):
        """Fired when the vcg_api client websocket disconnects.
        """
        if self.dc_client:
            async_to_sync(self.channel_layer.group_discard)(
                self.dc_client.group_name,
                self.channel_name
            )
            self.close()
            self.dc_client.last_disconnected = timezone.now()
            self.dc_client.status = 'disconnected'
            self.dc_client.save()
            log.info("Datacenter `{}` closed connection with code: {}".format(self.dc_client.name, close_code))

    def receive_json(self, content, **kwargs):
        """Fired when the vcg_api client websocket sends a message.

        Args:
            content (dict): the message payload
            **kwargs: extra parameters
        """
        now = timezone.now()
        log.debug("Received payload: {}".format(content))
        _request_id = content.pop('_request_id')
        evm_exception = content.pop('error', False)
        msg_log = DatacenterMessageLog.objects.filter(request_id=uuid.UUID(_request_id))

        # Sanity check
        if not msg_log.exists():
            log.error('No message log exists for request id: {}'.format(_request_id))
            return

        # Set the response payload
        msg_log.update(response_payload=content, response_timestamp=now, evm_exception=evm_exception)
        self.dc_client.last_message = now
        self.dc_client.save()

    def chain_message(self, message):
        """Message dispatcher to the underlying Datacenter.

        The dispatched message is logged and persisted in the database. Along with its payload,
        a unique request_id is attached. When the vcg_api client responds back, it also attaches
        the same uuid and the response is also logged.

        Args:
            message (dict): the parameters to send to the vcg_api.
                One required argument is `method` which is the signature of the vcg_api method to invoke.
                For a complete list of methods and their parameters check `vcg_api.ethereum.py`.

        Examples:
            To send a message to `DC1` to register a vcontainer, execute the following:

            >>> import arrow
            >>> from channels.layers import get_channel_layer
            >>> from ws.models import DatacenterClient
            >>>
            >>> layer = get_channel_layer()
            >>> dc_client = DatacenterClient.objects.get(name='DC1')
            >>> async_to_sync(layer.group_send)(dc_client.group_name, {
            >>>     'type': 'chain.message',
            >>>     'method': 'register_container',
            >>>     'vc_type': 'vm:openstack',
            >>>     'available': True,
            >>>     'vcpu': 4,
            >>>     'vram': 2,
            >>>     'vdisk': 20,
            >>>     'id': '71d68b44-625e-4aca-81e0-fa02c80da6b3',
            >>>     'ip_address': '192.168.1.180',
            >>>     'controller': 'http://192.168.1.100:8000',
            >>>     'created': arrow.get(timezone.now()).timestamp
            >>> })
        """

        # Create a new message log and attach the request_id uuid (if it is not present)
        if '_request_id' not in message.keys():
            msg_log = DatacenterMessageLog.objects.create(dc_client=self.dc_client, request_payload=message)
            message['_request_id'] = str(msg_log.request_id)

        # Send message to dc vcg_api client
        self.send_json(message)