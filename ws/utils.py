from datetime import datetime
import json

import logging

from django.utils import timezone
from eth_utils import encode_hex
from hexbytes import HexBytes
from web3.utils.datastructures import AttributeDict

from ws.models import ChainEvent

log = logging.getLogger(__name__)

def create_chain_event(event_args, decoded_event):
    """Creates a ChainEvent object

    Args:
        event_args (dict): the preprocessed event arguments
        decoded_event (dict): the rest event details

    """
    return ChainEvent.objects.create(
        name=decoded_event['event'],
        args=event_args,
        log_index=decoded_event['logIndex'],
        tx_index=decoded_event['transactionIndex'],
        tx_hash=decoded_event['transactionHash'].hex(),
        block_hash=decoded_event['blockHash'].hex(),
        block_number=decoded_event['blockNumber'],
        address=decoded_event['address']
    )


def chain_event_to_slarc(chain_event):
    ret = {}
    ret['id'] = chain_event.id
    ret['name'] = chain_event.name
    ret['args'] = {}
    ret['log_index'] = chain_event.log_index
    ret['tx_index'] = chain_event.tx_index
    ret['tx_hash'] = chain_event.tx_hash
    ret['block_number'] = chain_event.block_number
    ret['block_hash'] = chain_event.block_hash
    ret['address'] = chain_event.address
    ret['created'] = chain_event.created.utcnow().isoformat()

    ret['args']['owner'] = stringify(chain_event.args['owner']) if 'owner' in chain_event.args else None
    ret['args']['vcTag'] = stringify(chain_event.args['vcTag']) if 'vcTag' in chain_event.args else None
    ret['args']['timestamp'] = stringify(chain_event.args['timestamp']) if 'timestamp' in chain_event.args else None
    ret['args']['newStatus'] = chain_event.args['newStatus'] if 'newStatus' in chain_event.args else None
    ret['args']['oldStatus'] = chain_event.args['oldStatus'] if 'oldStatus' in chain_event.args else None
    ret['args']['from'] = stringify(chain_event.args['from']) if 'from' in chain_event.args else None
    ret['args']['to'] = stringify(chain_event.args['to']) if 'to' in chain_event.args else None
    ret['args']['price'] = chain_event.args['price'] if 'price' in chain_event.args else None
    ret['args']['invoice'] = chain_event.args['invoice'] if 'invoice' in chain_event.args else None
    return json.dumps(ret)


def stringify(obj):
    if isinstance(obj, HexBytes):
        return str(obj.hex())
    elif isinstance(obj, AttributeDict):
        return str(dict(obj))
    elif isinstance(obj, bytes):
        return str(encode_hex(obj))
    elif isinstance(obj, datetime):
        return obj.replace(tzinfo=timezone.utc).isoformat()
    else:
        return str(obj)