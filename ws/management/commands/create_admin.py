from django.contrib.auth.models import User
from django.core.management import BaseCommand


class Command(BaseCommand):
    help = 'Creates the admin superuser'

    def handle(self, *args, **options):
        user = User.objects.filter(username='admin')

        if user.exists():
            self.stdout.write('Admin user already exists')
        else:
            self.stdout.write('Creating admin superuser...')
            User.objects.create_superuser('admin', 'tomaras@synelixis.com', '12345678')
            self.stdout.write(self.style.SUCCESS('Admin successfully created'))