from django.conf import settings
from django.core.management import BaseCommand
from django_ethereum_events.models import MonitoredEvent

contract_address = settings.CONTRACT_ADDRESS
contract_abi = settings.CONTRACT_ABI

DEFAULT_EVENTS = [
    ('DatacenterRegistered', contract_address, contract_abi, 'ws.chainevent_receivers.DatacenterRegisteredEventReceiver'),
    ('ContainerRegistered', contract_address, contract_abi, 'ws.chainevent_receivers.ContainerRegisteredEventReceiver'),
    ('ContainerAvailabilityChanged', contract_address, contract_abi, 'ws.chainevent_receivers.ContainerAvailabilityChangedEventReceiver'),
    ('ContainerMigrationPending', contract_address, contract_abi, 'ws.chainevent_receivers.ContainerMigrationPendingEventReceiver'),
    ('ContainerMigrated', contract_address, contract_abi, 'ws.chainevent_receivers.ContainerMigratedEventReceiver')
]


class Command(BaseCommand):
    help = 'Creates the standard MonitoredEvent objects in the do not exist.'

    def handle(self, *args, **options):
        monitored_events = MonitoredEvent.objects.all()
        for event in DEFAULT_EVENTS:

            if not monitored_events.filter(name=event[0], contract_address__iexact=event[1]).exists():
                self.stdout.write('Creating monitor for event {} at {}'.format(event[0], event[1]))

                MonitoredEvent.objects.register_event(*event)

        self.stdout.write(self.style.SUCCESS('Events are up to date'))
