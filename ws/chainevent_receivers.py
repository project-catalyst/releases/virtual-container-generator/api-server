import json
import logging

import pytz
from datetime import datetime

from django.conf import settings
from django.db.models import Q
from django_ethereum_events.chainevents import AbstractEventReceiver
from django_ethereum_events.utils import HexJsonEncoder
from eth_utils import encode_hex
from rest_framework.renderers import JSONRenderer

from api.serializers import ChainEventSerializer
from api.tasks import transfer_ether_task, notify_slarc, register_UUID_mapping
from ws.models import DatacenterClient
from ws.utils import create_chain_event, chain_event_to_slarc

log = logging.getLogger(__name__)

class DatacenterRegisteredEventReceiver(AbstractEventReceiver):
    """Fired when a `DatacenterRegistered` is logged from the blockchain.
    """
    def save(self, decoded_event):
        decoded_event_dict = dict(decoded_event)
        args = dict(decoded_event_dict.pop('args'))

        # Preprocess the arguments
        args['timestamp'] = datetime.fromtimestamp(args['timestamp'], pytz.UTC)

        # Create the event instance
        create_chain_event(args, decoded_event_dict)

        if not DatacenterClient.objects.filter(Q(name=args['name']) | Q(address__iexact=args['wallet'])).exists():
            # Create the datacenter client entry
            client = DatacenterClient.objects.create(address=args['wallet'], name=args['name'])

            # Send the DC address some ether
            transfer_ether_task.delay(settings.WALLET_ADDRESS, client.address, settings.ETHER_TRANSFER_AMOUNT)


class ContainerRegisteredEventReceiver(AbstractEventReceiver):
    """Fired when a `ContainerRegistered` is logged from the blockchain.
    """
    def save(self, decoded_event):
        decoded_event_dict = dict(decoded_event)
        args = dict(decoded_event_dict.pop('args'))

        # Preprocess the arguments
        args['owner'] = args['owner']
        args['timestamp'] = datetime.fromtimestamp(args['timestamp'], pytz.UTC)
        args['vcTag'] = encode_hex(args['vcTag'])

        # Create the event instance
        chain_event = create_chain_event(args, decoded_event_dict)

        # Notify SLARC
        notify_slarc.apply_async(kwargs={'chain_event': chain_event_to_slarc(chain_event)})
        register_UUID_mapping.apply_async(kwargs={'vc_tag': args['vcTag']})


class ContainerAvailabilityChangedEventReceiver(AbstractEventReceiver):
    """Fired when a `ContainerAvailabilityChanged` is logged from the blockchain.
    """
    def save(self, decoded_event):
        decoded_event_dict = dict(decoded_event)
        args = dict(decoded_event_dict.pop('args'))

        # Preprocess the arguments
        args['timestamp'] = datetime.fromtimestamp(args['timestamp'], pytz.UTC)
        args['vcTag'] = encode_hex(args['vcTag'])

        # Create the event instance
        chain_event = create_chain_event(args, decoded_event_dict)

        # Notify SLARC
        notify_slarc.apply_async(kwargs={'chain_event': chain_event_to_slarc(chain_event)})


class ContainerMigrationPendingEventReceiver(AbstractEventReceiver):
    """Fired when a `ContainerMigrationPending` is logged from the blockchain.
    """
    def save(self, decoded_event):
        decoded_event_dict = dict(decoded_event)
        args = dict(decoded_event_dict.pop('args'))

        # Preprocess the arguments
        args['from'] = args['from']
        args['to'] = args['to']
        args['timestamp'] = datetime.fromtimestamp(args['timestamp'], pytz.UTC)
        args['vcTag'] = encode_hex(args['vcTag'])

        # Create the event instance
        chain_event = create_chain_event(args, decoded_event_dict)

        # Notify SLARC
        notify_slarc.apply_async(kwargs={'chain_event': chain_event_to_slarc(chain_event)})
        register_UUID_mapping.apply_async(kwargs={'vc_tag': args['vcTag']})


class ContainerMigratedEventReceiver(AbstractEventReceiver):
    """Fired when a `ContainerMigrated` is logged from the blockchain.
    """
    def save(self, decoded_event):
        decoded_event_dict = dict(decoded_event)
        args = dict(decoded_event_dict.pop('args'))

        # Preprocess the arguments
        args['from'] = args['from']
        args['to'] = args['to']
        args['timestamp'] = datetime.fromtimestamp(args['timestamp'], pytz.UTC)
        args['vcTag'] = encode_hex(args['vcTag'])

        # Create the event instance
        chain_event = create_chain_event(args, decoded_event_dict)

        # Notify SLARC
        notify_slarc.apply_async(kwargs={'chain_event': chain_event_to_slarc(chain_event)})
        register_UUID_mapping.apply_async(kwargs={'vc_tag': args['vcTag']})