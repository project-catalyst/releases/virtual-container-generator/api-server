import json
import os
import urllib

import requests
from celery.task import task
from django.conf import settings

from api.exceptions import OperationTimeout
from api.models import UUIDMapping
from api.utils import transfer_ether_amount, wait_for_transaction_mined
from api.views import ContainerDetails


@task
def transfer_ether_task(sender, recipient, amount):
    """Blocking task that transfers ether to the recipient.

    Args:
        recipient: hexstring address of recipient
        amount (int): the ether amount

    Returns:
        True if the transfer was successfully, False otherwise
    """
    tx_hash = transfer_ether_amount(sender, recipient, amount)

    # blocks for 60 seconds max (15 seconds on average)
    try:
        wait_for_transaction_mined(tx_hash, interval=1000, max_retries=60)
        return True
    except OperationTimeout:
        return False


@task
def notify_slarc(chain_event):
    try:
        server = os.getenv('SLARC_IP', settings.SLARC_IP)
        port = os.getenv('SLARC_PORT', 80)
        endpoint = os.getenv('SLARC_NOTIFY_API', '/api/events/external/vcg')
        url = urllib.parse.urljoin("{}://{}:{}".format("http", server, port), endpoint)
        res = requests.post(url=url,
                            headers={"Content-Type": "application/json"},
                            json=chain_event)
        res.raise_for_status()
        if res.status_code in [200, 201, 204]:
            return True
        else:
            return False

    except Exception as e:
        print(e)
    return False


@task
def register_UUID_mapping(vc_tag):
    """
    Registers a UUID in the DB together with its
    :param vc_tag: The vc_tag to register
    :return: a UUID Mapping object
    """
    uuids = UUIDMapping.objects.filter(vc_tag=vc_tag)
    if uuids.count() > 0:
        return True
    _candidate = ContainerDetails(kwargs={'vc_tag': vc_tag}).retrieve(request=None).data
    mapping =  UUIDMapping.objects.create(
        uuid=_candidate['id'],
        vc_tag=vc_tag,
        type=_candidate['vc_type'],
    )
    return True