from django.contrib import admin

# Register your models here.
from api import models


class UUIDMappingAdmin(admin.ModelAdmin):
    list_display = ['uuid', 'vc_tag', 'type']
    list_filter = ['uuid']
    ordering = ['id']
    search_fields = ['uuid', 'vc_tag']


admin.site.register(models.UUIDMapping, UUIDMappingAdmin)