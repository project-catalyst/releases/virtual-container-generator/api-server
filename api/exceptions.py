class VMException(Exception):
    """Raised when the ethereum client raises an error"""
    pass


class MessageLogTimeout(Exception):
    pass


class OperationTimeout(Exception):
    pass
