from datetime import datetime

import arrow
import pytz
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.http import Http404
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from eth_utils import encode_hex, is_hex_address, is_0x_prefixed, add_0x_prefix
from rest_framework.exceptions import ParseError
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from web3 import Web3

from api.exceptions import MessageLogTimeout, VMException
from api.models import UUIDMapping
from api.serializers import ChainEventSerializer, RegisterContainerSerializer, PendingMigrateContainerSerializer, \
    ConfirmMigrateContainerSerializer, ChangeContainerAvailabilitySerializer, ContainerSerializer, \
    PendingMigrationContainerSerializer, DatacenterSerializer, ContainerFlavorSerializer, TransactionToVcTagSerializer, \
    RegisterDatacenterSerializer, TransactionHashSerializer, TransactionContainerSerializer, AllVCTagsSerializer
from api.utils import poll_for_result
from api.validators import is_bytes32_hex
from ws.catalyst_ethereum import CatalystEthereumClient
from ws.models import ChainEvent, DatacenterClient, DatacenterMessageLog


VC_TAG_PARAMETER = openapi.Parameter(
    name='vc_tag', in_=openapi.IN_PATH,
    type=openapi.TYPE_STRING,
    description="The vcTag of the vcontainer",
    required=True
)

ADDRESS_PARAMETER = openapi.Parameter(
    name='address', in_=openapi.IN_PATH,
    type=openapi.TYPE_STRING,
    description="Wallet public key of the DC",
    required=True
)

DC_NAME_PARAMETER = openapi.Parameter(
    name='dc_name', in_=openapi.IN_PATH,
    type=openapi.TYPE_STRING,
    description="Name of the datacenter to forward the request to",
    required=True
)

TRANSACTION_PARAMETER = openapi.Parameter(
    name='tx_hash', in_=openapi.IN_PATH,
    type=openapi.TYPE_STRING,
    description="The generated transaction hash",
    required=True
)

SERVER_RESPONSES = {
    status.HTTP_200_OK: openapi.Response('The generated transaction hash', TransactionHashSerializer),
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed',
    status.HTTP_403_FORBIDDEN: 'The smart contract rolled back (declined) the transaction',
}

DATACENTER_RESPONSES = {
    status.HTTP_200_OK: openapi.Response('The generated transaction hash', TransactionHashSerializer),
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed',
    status.HTTP_403_FORBIDDEN: 'The smart contract rolled back (declined) the transaction',
    status.HTTP_408_REQUEST_TIMEOUT: 'The underlying datacenter client did not response in time'
}


class ContainerDetails(RetrieveAPIView):
    """Retrieves the details of the vcontainer from the blockchain.
    """
    serializer_class = ContainerSerializer
    chain_method = 'get_container'

    @swagger_auto_schema(manual_parameters=[VC_TAG_PARAMETER])
    def get(self, request, *args, **kwargs):
        return super(ContainerDetails, self).get(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        vc_tag = self.kwargs.get('vc_tag')

        if not is_bytes32_hex(vc_tag):
            raise ParseError(detail='Invalid vc_tag format')

        client = CatalystEthereumClient()
        response = client.handle_message({
            'method': self.chain_method,
            'vc_tag': vc_tag
        })

        # Response is an array and the position of the arguments is the same as the solidity return statement
        container = {
            'vc_type': response[0],
            'available': response[1],
            'id': response[2],
            'ip_address': response[3],
            'host': response[4],
            'controller': response[5],
            'owner': response[6],
            'created': arrow.get(response[7]),
            'updated': arrow.get(response[8])
        }

        serializer = self.serializer_class(container)
        return Response(serializer.data)


class ContainerFlavorDetails(RetrieveAPIView):
    """Retrieves the flavor details of the vcontainer from the blockchain.
    """
    serializer_class = ContainerFlavorSerializer
    chain_method = 'get_container_flavor'

    @swagger_auto_schema(manual_parameters=[VC_TAG_PARAMETER])
    def get(self, request, *args, **kwargs):
        return super(ContainerFlavorDetails, self).get(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        vc_tag = self.kwargs.get('vc_tag')

        if not is_bytes32_hex(vc_tag):
            raise ParseError(detail='Invalid vc_tag format')

        client = CatalystEthereumClient()
        response = client.handle_message({
            'method': self.chain_method,
            'vc_tag': vc_tag
        })

        # Response is an array and the position of the arguments is the same as the solidity return statement
        container = {
            'vcpu': response[0],
            'vram': response[1],
            'vdisk': response[2],
        }

        serializer = self.serializer_class(container)
        return Response(serializer.data)


class ContainerPendingMigrationDetails(RetrieveAPIView):
    """Retrieves the pending migrate information of the given container from the blockchain (if any).
    """
    serializer_class = PendingMigrationContainerSerializer
    chain_method = 'get_pending_migration'

    @swagger_auto_schema(manual_parameters=[VC_TAG_PARAMETER])
    def get(self, request, *args, **kwargs):
        return super(ContainerPendingMigrationDetails, self).get(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        vc_tag = self.kwargs.get('vc_tag')

        if not is_bytes32_hex(vc_tag):
            raise ParseError(detail='Invalid vc_tag format')

        client = CatalystEthereumClient()
        response = client.handle_message({
            'method': self.chain_method,
            'vc_tag': vc_tag
        })

        # Response is an array and the position of the arguments is the same as the solidity return statement
        container = {
            'vc_tag': encode_hex(response[0]),
            'from_address': response[1],
            'to_address': response[2],
            'price': response[3],
            'invoice': response[4],
        }

        serializer = self.serializer_class(container)
        return Response(serializer.data)


class DatacenterDetails(RetrieveAPIView):
    """Retrieves the datacenter details from the blockchain.
    """
    serializer_class = DatacenterSerializer
    chain_method = 'get_datacenter'

    @swagger_auto_schema(manual_parameters=[ADDRESS_PARAMETER])
    def get(self, request, *args, **kwargs):
        return super(DatacenterDetails, self).get(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        address = self.kwargs.get('address')

        if not is_0x_prefixed(address) or not is_hex_address(address):
            raise ParseError(detail='Invalid address format')

        # Accept addresses in no checksumed form
        address = Web3.toChecksumAddress(address)

        client = CatalystEthereumClient()
        response = client.handle_message({
            'method': self.chain_method,
            'address': address
        })

        # Response is an array and the position of the arguments is the same as the solidity return statement
        container = {
            'name': response[0],
            'wallet': response[1],
            'registered': response[2],
        }

        serializer = self.serializer_class(container)
        return Response(serializer.data)


class ContainerHistory(ListAPIView):
    """Retrieves the history of the operations performed on the vcontainer as they are persisted in the blockchain.
    """
    serializer_class = ChainEventSerializer
    lookup_field = 'vc_tag'

    def get_queryset(self):
        qs = ChainEvent.objects.filter(args__vcTag__iexact=self.vc_tag)
        return qs

    @swagger_auto_schema(manual_parameters=[VC_TAG_PARAMETER])
    def get(self, request, *args, **kwargs):
        self.vc_tag = self.kwargs.get(self.lookup_field)

        if not is_bytes32_hex(self.vc_tag):
            raise ParseError(detail='Invalid vc_tag format')
        return super(ContainerHistory, self).get(request, *args, **kwargs)


class TransactionToVCTag(RetrieveAPIView):
    """Retrieves the vcTag that the given tx_hash created (if any).
    """
    serializer_class = TransactionContainerSerializer

    @swagger_auto_schema(manual_parameters=[TRANSACTION_PARAMETER])
    def get(self, request, *args, **kwargs):
        return super(TransactionToVCTag, self).get(request, *args, **kwargs)

    def get_object(self):
        tx_hash = self.kwargs.get('tx_hash')
        client = CatalystEthereumClient()
        tx_receipt = client.web3.eth.getTransactionReceipt(tx_hash)

        if not tx_receipt:
            raise Http404

        rich_logs = client.contract.events.ContainerRegistered().processReceipt(tx_receipt)

        if not rich_logs:
            raise Http404

        return {
            'vc_tag': add_0x_prefix(rich_logs[0].args.vcTag.hex()),
            'owner': rich_logs[0].args.owner,
            'created': datetime.fromtimestamp(rich_logs[0].args.timestamp, pytz.UTC),
            'block_number': rich_logs[0].blockNumber,
            'block_hash': rich_logs[0].blockHash.hex()
        }


class RegisterDatacenter(APIView):
    """Registers a new datacenter inside the blockchain smart contract.
    """
    serializer_class = RegisterDatacenterSerializer
    chain_method = 'register_datacenter'

    @swagger_auto_schema(request_body=serializer_class, responses=SERVER_RESPONSES)
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            vd = serializer.validated_data

            # Preprocess before sending ws request to dc
            payload = dict(vd)
            payload['method'] = self.chain_method

            client = CatalystEthereumClient()
            tx_hash = client.handle_message(payload)
            return Response(TransactionHashSerializer({'tx_hash': tx_hash}).data)
        return Response(serializer.errors, status=400)


class RegisterContainer(APIView):
    serializer_class = RegisterContainerSerializer
    chain_method = 'register_container'
    lookup_field = 'dc_name'

    @swagger_auto_schema(manual_parameters=[DC_NAME_PARAMETER], request_body=serializer_class,
                         responses=DATACENTER_RESPONSES)
    def post(self, request, *args, **kwargs):
        dc_name = self.kwargs.get(self.lookup_field)
        dc_client = DatacenterClient.objects.filter(name=dc_name)

        if not dc_client.exists():
            return Response({'dc_name': 'Datacenter name invalid or not registered'}, status=404)
        dc_client = dc_client.first()

        if dc_client.status != 'connected':
            raise ParseError(detail='Datacenter {} is currently not reachable'.format(dc_name))

        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            vd = serializer.validated_data

            # Preprocess before sending ws request to dc
            payload = dict(vd)
            payload['type'] = 'chain.message'
            payload['method'] = self.chain_method
            payload['created'] = arrow.get(payload['created']).timestamp

            # Create the request_id uuid
            msg_log = DatacenterMessageLog.objects.create(dc_client=dc_client, request_payload=payload)
            request_id = str(msg_log.request_id)
            payload['_request_id'] = request_id

            # Send ws request
            layer = get_channel_layer()
            async_to_sync(layer.group_send)(dc_client.group_name, payload)

            try:
                response = poll_for_result(request_id)
            except VMException:
                return Response({'details': 'The transaction was rejected by the client'.format(dc_name)}, status=400)
            except MessageLogTimeout:
                dc_client.status = 'unknown'
                dc_client.save()
                return Response({'details': 'Datacenter {} did not respond in time'.format(dc_name)}, status=408)

            return Response({'tx_hash': response['response']}, status=200)
        return Response(serializer.errors, status=400)


class PendingMigrateContainer(APIView):
    serializer_class = PendingMigrateContainerSerializer
    chain_method = 'pending_migrate'

    @swagger_auto_schema(manual_parameters=[DC_NAME_PARAMETER, VC_TAG_PARAMETER], request_body=serializer_class,
                         responses=DATACENTER_RESPONSES)
    def post(self, request, *args, **kwargs):
        dc_name = self.kwargs.get('dc_name')
        vc_tag = self.kwargs.get('vc_tag')

        dc_client = DatacenterClient.objects.filter(name=dc_name)

        if not dc_client.exists():
            return Response({'dc_name': 'Datacenter name invalid or not registered'}, status=404)
        dc_client = dc_client.first()

        if dc_client.status != 'connected':
            raise ParseError(detail='Datacenter {} is currently not reachable'.format(dc_name))

        if not is_bytes32_hex(vc_tag):
            raise ParseError(detail='Invalid vc_tag format')

        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            vd = serializer.validated_data

            # Preprocess before sending ws request to dc
            payload = dict(vd)
            payload['type'] = 'chain.message'
            payload['method'] = self.chain_method
            payload['timestamp'] = arrow.get(payload['timestamp']).timestamp
            payload['vc_tag'] = vc_tag

            # Create the request_id uuid
            msg_log = DatacenterMessageLog.objects.create(dc_client=dc_client, request_payload=payload)
            request_id = str(msg_log.request_id)
            payload['_request_id'] = request_id

            # Send ws request
            layer = get_channel_layer()
            async_to_sync(layer.group_send)(dc_client.group_name, payload)

            try:
                response = poll_for_result(request_id)
            except VMException:
                return Response({'details': 'The transaction was rejected by the client'.format(dc_name)}, status=400)
            except MessageLogTimeout:
                dc_client.status = 'unknown'
                dc_client.save()
                return Response({'details': 'Datacenter {} did not respond in time'.format(dc_name)}, status=408)

            return Response({'tx_hash': response['response']}, status=200)
        return Response(serializer.errors, status=400)


class ConfirmMigrateContainer(APIView):
    """Confirms a pending to migrate action.
    """
    serializer_class = ConfirmMigrateContainerSerializer
    chain_method = 'migrate_container'

    @swagger_auto_schema(manual_parameters=[DC_NAME_PARAMETER, VC_TAG_PARAMETER], request_body=serializer_class,
                         responses=DATACENTER_RESPONSES)
    def post(self, request, *args, **kwargs):
        dc_name = self.kwargs.get('dc_name')
        vc_tag = self.kwargs.get('vc_tag')

        dc_client = DatacenterClient.objects.filter(name=dc_name)

        if not dc_client.exists():
            return Response({'dc_name': 'Datacenter name invalid or not registered'}, status=404)
        dc_client = dc_client.first()

        if dc_client.status != 'connected':
            raise ParseError(detail='Datacenter {} is currently not reachable'.format(dc_name))

        if not is_bytes32_hex(vc_tag):
            raise ParseError(detail='Invalid vc_tag format')

        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            vd = serializer.validated_data

            # Preprocess before sending ws request to dc
            payload = dict(vd)
            payload['type'] = 'chain.message'
            payload['method'] = self.chain_method
            payload['timestamp'] = arrow.get(payload['timestamp']).timestamp
            payload['vc_tag'] = vc_tag

            # Create the request_id uuid
            msg_log = DatacenterMessageLog.objects.create(dc_client=dc_client, request_payload=payload)
            request_id = str(msg_log.request_id)
            payload['_request_id'] = request_id

            # Send ws request
            layer = get_channel_layer()
            async_to_sync(layer.group_send)(dc_client.group_name, payload)

            try:
                response = poll_for_result(request_id)
            except VMException:
                return Response({'details': 'The transaction was rejected by the client'.format(dc_name)}, status=400)
            except MessageLogTimeout:
                dc_client.status = 'unknown'
                dc_client.save()
                return Response({'details': 'Datacenter {} did not respond in time'.format(dc_name)}, status=408)

            return Response({'tx_hash': response['response']}, status=200)
        return Response(serializer.errors, status=400)


class ChangeContainerAvailability(APIView):
    """Changes the availability of the given vcontainer.
    """
    serializer_class = ChangeContainerAvailabilitySerializer
    chain_method = 'change_availability'

    @swagger_auto_schema(manual_parameters=[DC_NAME_PARAMETER, VC_TAG_PARAMETER], request_body=serializer_class,
                         responses=DATACENTER_RESPONSES)
    def post(self, request, *args, **kwargs):
        dc_name = self.kwargs.get('dc_name')
        vc_tag = self.kwargs.get('vc_tag')

        dc_client = DatacenterClient.objects.filter(name=dc_name)

        if not dc_client.exists():
            return Response({'dc_name': 'Datacenter name invalid or not registered'}, status=404)
        dc_client = dc_client.first()

        if dc_client.status != 'connected':
            raise ParseError(detail='Datacenter {} is currently not reachable'.format(dc_name))

        if not is_bytes32_hex(vc_tag):
            raise ParseError(detail='Invalid vc_tag format')

        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            vd = serializer.validated_data

            # Preprocess before sending ws request to dc
            payload = dict(vd)
            payload['type'] = 'chain.message'
            payload['method'] = self.chain_method
            payload['timestamp'] = arrow.get(payload['timestamp']).timestamp
            payload['vc_tag'] = vc_tag

            # Create the request_id uuid
            msg_log = DatacenterMessageLog.objects.create(dc_client=dc_client, request_payload=payload)
            request_id = str(msg_log.request_id)
            payload['_request_id'] = request_id

            # Send ws request
            layer = get_channel_layer()
            async_to_sync(layer.group_send)(dc_client.group_name, payload)

            try:
                response = poll_for_result(request_id)
            except VMException:
                return Response({'details': 'The transaction was rejected by the client'.format(dc_name)}, status=400)
            except MessageLogTimeout:
                dc_client.status = 'unknown'
                dc_client.save()
                return Response({'details': 'Datacenter {} did not respond in time'.format(dc_name)}, status=408)

            return Response({'tx_hash': response['response']}, status=200)
        return Response(serializer.errors, status=400)


class AllVCTags(ListAPIView):
    """
    Gets all the VCTags
    """
    queryset = ChainEvent.objects.filter(name='ContainerRegistered')
    serializer_class = AllVCTagsSerializer

    def list(self, request, *args, **kwargs):
        ret = {"vc_tags": []}
        try:
            ret['vc_tags'] = [x.args['vcTag'] for x in self.get_queryset()]
            return Response(data=ret, status=200)
        except Exception as e:
            return Response(data={"details": "An error occurred: {}".format(str(e)) }, status=500)


class VCTagByID(ListAPIView):
    """
    Get a VCTag by querying its ID.
    """
    queryset = UUIDMapping.objects.all()
    serializer_class = AllVCTagsSerializer

    def list(self, request, *args, **kwargs):
        uuid = self.kwargs.get('uuid')
        _candidate = self.get_queryset().filter(uuid=uuid)
        if _candidate.count() == 0:
            return Response(data={'msg': 'Could not find a valid UUID <-> VC Tag matching query.'}, status=404)
        ret = {"vc_tags": [_candidate.last().vc_tag]}
        return Response(data=ret)