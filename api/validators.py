from eth_utils import is_0x_prefixed, is_hex


def is_bytes32_hex(value):
    """Validate that the input is a '0x' prefixed bytes32 hex string representation.

    Args:
        value: the object to test for

    Returns:
        True if test passes, False otherwise
    """
    if type(value) == str and is_0x_prefixed(value) and is_hex(value) and len(value) == 66:
        return True
    return False
