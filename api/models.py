from django.db import models


# Create your models here.
class UUIDMapping(models.Model):
    uuid = models.CharField(max_length=128, help_text="The UUID of the VC")
    vc_tag = models.CharField(max_length=66, help_text="The VC Tag")
    type = models.CharField(max_length=20, help_text="The type of the VC")