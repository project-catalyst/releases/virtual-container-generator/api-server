import math
from datetime import timedelta

from web3 import Web3

from .base import *

# =================================
#   DATABASE SETTINGS
# =================================
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'vcg_api_server',
        'USER': 'postgres',
        'PASSWORD': '12345678',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}

DEBUG = False

# Password validation
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# ==================================
#   LOGGING SETTINGS
# ==================================
LOGGING['handlers']['logfile']['filename'] = '/var/log/vcg_api_server/server.log'

# ==================================
#   REDIS SETTINGS
# ==================================
REDIS_IP = '127.0.0.1'
REDIS_PORT = 6379
REDIS_PASSWORD=None
REDIS_CELERY_DATABASE = 0       # Database number that celery uses as a broker
REDIS_CACHE_DATABASE = 1        # Database number that the django cache & session use as a backend
REDIS_DATA_DATABASE = 2         # Database number that the server uses for custom data storage
REDIS_ORM_CACHE_DATABASE = 3    # Database number used for ORM caching
REDIS_COMPRESSOR_DATABASE = 4   # Database number used for COMPRESSOR
REDIS_CHANNELS_DATABASE = 10    # Database number used for channels routing

ORM_CACHE_TIMEOUT = 86400  # Seconds before ORM cache invalidates (one day)
COMPRESSOR_TIMEOUT = 2592000  # Seconds before COMPRESSOR cache invalidates (30 days)

# ==================================
#   CACHE SETTINGS
# ==================================
CACHES = {
    'default': {
        'BACKEND': 'redis_cache.RedisCache',
        'LOCATION': '{}:{}'.format(REDIS_IP, REDIS_PORT),
        'OPTIONS': {
            'DB': REDIS_CACHE_DATABASE,
            'PASSWORD': REDIS_PASSWORD
        },
        'TIMEOUT': None,  # Never expire
    },
    'session_cache': {
        'BACKEND': 'redis_cache.RedisCache',
        'LOCATION': '{}:{}'.format(REDIS_IP, REDIS_PORT),
        'OPTIONS': {
            'DB': REDIS_CACHE_DATABASE,
            'PASSWORD': REDIS_PASSWORD
        },
        'TIMEOUT': 604800,  # one week
    },
    'compressor_cache': {
        'BACKEND': 'redis_cache.RedisCache',
        'LOCATION': '{}:{}'.format(REDIS_IP, REDIS_PORT),
        'OPTIONS': {
            'DB': REDIS_COMPRESSOR_DATABASE,
            'PASSWORD': REDIS_PASSWORD
        },
        'TIMEOUT': 2 * COMPRESSOR_TIMEOUT,
    },
    'orm_cache': {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://{}:{}/{}".format(REDIS_IP, REDIS_PORT, REDIS_ORM_CACHE_DATABASE),
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "PASSWORD": REDIS_PASSWORD
        },
        "TIMEOUT": 2 * ORM_CACHE_TIMEOUT
    }
}

# ==================================
#   SESSION SETTINGS
# ==================================
SESSION_ENGINE = "django.contrib.sessions.backends.cache"
SESSION_CACHE_ALIAS = "session_cache"


# =================================
#   CELERY SETTINGS
# =================================
if not REDIS_PASSWORD:
    BROKER_URL = 'redis://{}:{}/{}'.format(REDIS_IP, REDIS_PORT, REDIS_CELERY_DATABASE)
    CELERY_RESULT_BACKEND = 'redis://{}:{}/{}'.format(REDIS_IP, REDIS_PORT, REDIS_CELERY_DATABASE)
else:
    BROKER_URL = 'redis://:{}@{}:{}/{}'.format(REDIS_PASSWORD, REDIS_IP, REDIS_PORT, REDIS_CELERY_DATABASE)
    CELERY_RESULT_BACKEND = 'redis://:{}@{}:{}/{}'.format(REDIS_PASSWORD, REDIS_IP, REDIS_PORT, REDIS_CELERY_DATABASE)

CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
djcelery.setup_loader()


# =================================
#   CELERY BEAT SETTINGS
# =================================
CELERY_TIMEZONE = 'UTC'
CELERYBEAT_SCHEDULE = {
    'ethereum_events': {
        'task': 'django_ethereum_events.tasks.event_listener',
        'schedule': timedelta(seconds=20)
    }
}


# =================================
#   CHANNELS SETTINGS
# =================================
if not REDIS_PASSWORD:
    CHANNELS_BROKER = 'redis://{}:{}/{}'.format(REDIS_IP, REDIS_PORT, REDIS_CHANNELS_DATABASE)
else:
    CHANNELS_BROKER = 'redis://:{}@{}:{}/{}'.format(REDIS_PASSWORD, REDIS_IP, REDIS_PORT, REDIS_CHANNELS_DATABASE)

ASGI_APPLICATION = 'vcg_api_server.routing.application'
CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            "hosts": [
                CHANNELS_BROKER
            ],
        },
    },
}


# ==================================
#   ORM CACHE (CACHALOT)
# ==================================
CACHALOT_ENABLED = True
CACHALOT_CACHE = 'orm_cache'
CACHALOT_CACHE_RANDOM = True
CACHALOT_INVALIDATE_RAW = True
CACHALOT_TIMEOUT = ORM_CACHE_TIMEOUT


# ==================================
#   COMPRESSOR SETTINGS
# ==================================
COMPRESS_ENABLED = True
COMPRESS_CACHE_BACKEND = "compressor_cache"

COMPRESS_CSS_FILTERS = [
    'compressor.filters.css_default.CssAbsoluteFilter',
    'compressor.filters.cssmin.CSSCompressorFilter'
]

COMPRESS_JS_FILTERS = [
    'compressor.filters.jsmin.JSMinFilter',
]


# =================================
#   ETHEREUM SETTINGS
# =================================

# Wallet address (account) of the operator
WALLET_ADDRESS = Web3.toChecksumAddress("0xa4dfb027fa681d0c6ef3ab46dabc73bb7c2df48e")

# Wallet password. KEEP THIS SECRET!
WALLET_PASSWORD = "pwdnode"

# Ethereum node host
ETHEREUM_HOST = "localhost"

# Ethereum node port
ETHEREUM_PORT = 7545

# If using a PoA consensus engine
ETHEREUM_POA = True

# Address of the deployed smart contract
CONTRACT_ADDRESS = Web3.toChecksumAddress("0x6d4a5a876d09f8a381993d7f8df9d3764c5a2491")
CONTRACT_ABI = json.loads(CONTRACT_ABI_RAW)

EXPLORER_ADDRESS = "0x324ED4a24121bF8dec2E34bd38b238448EcEe84b"
EXPLORER_IP = "172.25.0.105"
EXPLORER_PORT = 8545

ETHER_TRANSFER_AMOUNT = math.pow(10, 5)

# =================================
#   ETHEREUM-EVENTS SETTINGS
# =================================
ETHEREUM_NODE_HOST = ETHEREUM_HOST
ETHEREUM_NODE_PORT = ETHEREUM_PORT
ETHEREUM_NODE_SSL = False
ETHEREUM_GETH_POA = ETHEREUM_POA

