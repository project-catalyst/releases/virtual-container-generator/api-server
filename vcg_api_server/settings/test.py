from .base import *

CELERY_ALWAYS_EAGER = True

ASGI_APPLICATION = 'vcg_api_server.routing.application'
CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels.layers.InMemoryChannelLayer',
    },
}


# =================================
#   ETHEREUM SETTINGS
# =================================
# Every address needs to be in checksum format
# e.g.: use Web3.toChecksumAddress() to convert an address to its checksum form

# Wallet address (account) of the operator
#WALLET_ADDRESS = "0xaf5eef68e74857b8e9997009769ad7b7ff428596"
WALLET_ADDRESS = "0x90F8bf6A479f320ead074411a4B0e7944Ea8c9C1"

# Wallet password. KEEP THIS SECRET!
WALLET_PASSWORD = "pwdapiserver"

# Ethereum node host
ETHEREUM_HOST = "localhost"

# Ethereum node port
ETHEREUM_PORT = 7545

# Address of the deployed smart contract
#CONTRACT_ADDRESS = "0xb717286cfe0c148205a3ae7faba9aa9b610ecf0b"
CONTRACT_ADDRESS = "0xCfEB869F69431e42cdB54A4F4f105C19C080A601"

CONTRACT_ABI = json.loads(CONTRACT_ABI_RAW)

#EXPLORER_ADDRESS = "0x324ed4a24121bf8dec2e34bd38b238448ecee84b"
EXPLORER_ADDRESS = "0xFFcf8FDEE72ac11b5c542428B35EEF5769C409f0"
EXPLORER_IP = "localhost"
EXPLORER_PORT = 7545

ETHER_TRANSFER_AMOUNT = 1

