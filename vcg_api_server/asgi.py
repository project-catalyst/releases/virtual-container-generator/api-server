"""
ASGI entrypoint. Configures Django and then runs the application
defined in the ASGI_APPLICATION setting.
"""

import os
import django
from channels.routing import get_default_application

from vcg_api_server.settings import get_env_setting

settings_file = get_env_setting('DJANGO_SETTINGS_MODULE')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", settings_file)
django.setup()
application = get_default_application()