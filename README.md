# Catalyst VCG API Server

### Required software
1. Docker [engine](https://docs.docker.com/install/linux/docker-ce/ubuntu/).
2. Docker [compose](https://docs.docker.com/compose/install/).

## Prerequisites
1. The **PoA** ethereum network needs to be present and running via ```docker-compose```. The repository and install instructions can be found [here](http://git.synelixis.com/pops/blockchain-testnet).
2. The **VCG smart contract(s)** must reside inside the blockchain. You can deploy the smart contracts remotely by following the steps in the vcg_sc [repo](http://git.synelixis.com/catalyst/vc_generator/vcg_sc).

## Deployment
The following settings must be updated with the information generated from the previous section:

1. **config/.env**: Holds the settings passed to ```docker-compose```.
2. **vcg_api_server/settings/prod.py**: Holds the settings that will be used by django during ```docker-compose```.

The wallet account of the **vcg_api_server** is pre-generated and is stored in ``config/accounts``.

To launch the service, run
```bash
bash config/launch.sh
```

## Providing ETHER to the wallet address
For the vcg_api_server and the connecting DCs to make transactions, some gas must be paid. __This translates to some ether been spend__.

Because the blockchain uses the **Proof of Authority** consensus protocol, a helper django management method has been created to transfer ETHER from the ```geth-explorer``` wallet address to the ```vcg_api_server``` wallet ( or any other).

To transfer a large amount of ether ( 10^20 ) to the **vcg_api_server**, execute the following from inside the **vcg-api-server** docker container:
```bash
cd /opt/vcg_api_server
python3 manage.py transfer_ether --settings=vcg_api_server.settings.prod
```

Datacenters that register are automatically send a configurable amount of ether, so the mentioned command really needs to run only once. An optional ``-t, --target`` and ``-e, --ether`` argument can be passed to the command to specify to who to send ether to and to which amount.

## Exposed ports
1. port ``5100`` is the django application, ``/api/swagger`` for swagger documentation
2. port ``8000`` is the blockchain visualization explorer